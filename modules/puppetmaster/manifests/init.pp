class puppetmaster {
  include apache2

  package { 'puppetmaster':
    ensure => installed,
  }
  file { '/etc/puppet/puppetdb.conf':
    source => 'puppet:///modules/puppetmaster/puppetdb.conf'
  }

  ferm::rule { 'dsa-puppet':
    description => 'Allow puppet access',
    domain      => '(ip ip6)',
    rule        => '&SERVICE_RANGE(tcp, 8140, $HOST_DEBIAN)',
  }

  file { '/srv/puppet.debian.org/puppet-facts':
    ensure => directory
  }
  concat { '/srv/puppet.debian.org/puppet-facts/onionbalance-services.yaml':
  }
  Concat::Fragment <<| tag == 'onionbalance-services.yaml' |>>

  file { '/etc/cron.d/puppet-update-fastly-ips': ensure => absent, }
  file { '/etc/cron.d/update-fastly-ips': ensure => absent, }
  concat::fragment { 'puppet-crontab---fastly-ips':
    target  => '/etc/cron.d/puppet-crontab',
    content => @(EOF)
      @daily  root  /usr/local/bin/update-fastly-ips /srv/puppet.debian.org/puppet-facts/fastly_ranges.yaml
      | EOF
  }
  file { '/usr/local/bin/update-fastly-ips':
    source => 'puppet:///modules/puppetmaster/update-fastly-ips.sh',
    mode   => '0555',
  }

  file { '/etc/logrotate.d/puppetdb':
    ensure => absent,
  }
}
