# set up autofs mounts and symlinks for bytemark nfs clients
class autofs::bytemark {
  include autofs::common

  file { '/etc/auto.master.d/dsa.autofs':
    source => 'puppet:///modules/autofs/bytemark/auto.master.d-dsa.autofs',
    notify => Exec['autofs reload']
  }
  file { '/etc/auto.dsa':
    source => 'puppet:///modules/autofs/bytemark/auto.dsa',
    notify => Exec['autofs reload']
  }

  file { '/srv/mirrors': ensure => directory }
  file { '/srv/mirrors/debian': ensure => symlink, target => '/auto.dsa/debian' }
  file { '/srv/mirrors/debian-backports': ensure => absent }
  file { '/srv/mirrors/debian-buildd': ensure => symlink, target => '/auto.dsa/debian-buildd' }
  file { '/srv/mirrors/debian-debug': ensure => symlink, target => '/auto.dsa/debian-debug' }
  file { '/srv/mirrors/debian-ports': ensure => symlink, target => '/auto.dsa/debian-ports' }
  file { '/srv/mirrors/debian-security': ensure => symlink, target => '/auto.dsa/debian-security' }
}
