class roles::sreview {
  include apache2
  ssl::service { 'sreview.debian.net': notify  => Exec['service apache2 reload'], key => true, }

  dsa_systemd::linger { 'sreview': }
}
