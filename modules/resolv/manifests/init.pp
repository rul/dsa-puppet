class resolv(
  Array[Stdlib::IP::Address] $nameservers = [],
  Array[String] $searchpaths = [],
  Array[String] $resolvoptions = [],
) {

  $ns = $facts['unbound'] ? {
    true    => ['127.0.0.1'],
    default => $nameservers,
  }

  $opts = $facts['unbound'] ? {
    true    => ['edns0'] + $resolvoptions,
    default => $resolvoptions,
  }

  file { '/etc/resolv.conf':
      content => template('resolv/resolv.conf.erb');
  }

  file { '/etc/dhcp/dhclient-enter-hooks.d/puppet-no-resolvconf':
    content  => @("EOF"),
                   make_resolv_conf() {
                     :
                   }
                   | EOF
    mode => '555',
    ensure => ($dhclient and $unbound) ? {
      true     => 'present',
      false    => 'absent',
    }
  }
}
