# LVM config for the blades that make up ganeti.csail.debian.org
class profile::lvm::ganeti_csail {
  class { 'lvm':
    global_filter  => '[ "a|^/dev/sda[0-9]*$|", "r/.*/" ]',
  }
}
