class roles::bugs_web {
  class { 'apache2':
    rate_limit => true,
  }

  ssl::service { 'bugs.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  ferm::rule { 'dsa-bugs-abusers':
    prio => '005',
    rule => 'saddr (220.243.135/24 220.243.136/24) DROP',
  }

  file { '/usr/local/lib/site_perl':
    ensure => directory,
    owner  => 'root',
    group  => 'staff',
    mode   => '2755',
  }
  file { '/usr/local/lib/site_perl/Debbugs':
    ensure => link,
    target => '/srv/bugs.debian.org/perl/Debbugs',
  }
  file { '/etc/debbugs':
    ensure => link,
    target => '/srv/bugs.debian.org/etc',
  }
}
