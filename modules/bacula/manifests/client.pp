# our bacula client configuration
#
# this mostly configures the file daemon, but also firewall rules and
# fragments to sent to the other servers.
#
# @param director_server  director server (address) that controls this client's backups
# @param storage_server   storage server (address) to use for this client
# @param port_fd          port that bacula-fd listens on
# @param client_name      bacula client name for this instance
# @param ensure           present or absent
# @param volume_retention_full  how long to keep volumes with full backups
# @param volume_retention_diff  how long to keep volumes with differential backups
# @param volume_retention_inc   how long to keep volumes with incremental backups
# @param file_retention         how long to keep information about which files are in which volumes/jobs
# @param job_retention          how long to keep job records
class bacula::client(
  Stdlib::Host $director_server,
  Stdlib::Host $storage_server,
  Integer $port_fd = 9102,
  String $client_name = "${::fqdn}-fd",
  Enum['present', 'absent'] $ensure = defined(Class['bacula::not_a_client']) ? { true => 'absent', default => 'present' },
  String $volume_retention_full = '100 days',
  String $volume_retention_diff = '50 days',
  String $volume_retention_inc  = '30 days',
  String $file_retention = '30 days',
  String $job_retention = $volume_retention_full,
) inherits bacula {
  $package_ensure = $ensure ? { 'present' => 'installed', 'absent' => 'purged' }
  $service_ensure = $ensure ? { 'present' => 'running', 'absent'  => 'stopped' }
  $service_enable = $ensure ? { 'present' => true, 'absent' => false }
  $reverse_ensure = $ensure ? { 'present' => 'absent', 'absent' => 'present' }
  $directory_ensure = $ensure ? { 'present' => 'directory', 'absent' => 'absent' }

  $client = $::fqdn

  file { '/etc/bacula/fd-conf.d':
    ensure  => $directory_ensure,
    mode    => '0755',
    group   => bacula,
    purge   => true,
    force   => true,
    recurse => true,
    notify  => Exec['bacula-fd restart-when-idle'],
  }

  if $ensure == 'present' {
    Bacula::Client::Director <<| tag == "bacula::to-fd::${director_server}" |>> {
      before => Exec['bacula-fd restart-when-idle'],
    }

    @@bacula::storage::client { $client:
      tag                   => "bacula::to-storage::${storage_server}",
      client                => $client,
      director_server       => $director_server,
      volume_retention_full => $volume_retention_full,
      volume_retention_diff => $volume_retention_diff,
      volume_retention_inc  => $volume_retention_inc,
    }

    @@concat::fragment { "bacula-dsa-client-list::${client}":
      tag     => $bacula::tag_bacula_dsa_client_list,
      target  => $bacula::bacula_dsa_client_list,
      content => $client,
    }

    # allow access from director
    Ferm::Rule::Simple <<| tag == "bacula::director-to-fd::${director_server}" |>> {
      port => $port_fd,
    }

    # get access to the storage
    @@ferm::rule::simple { "bacula::fd-to-storage::${client}":
      tag         => "bacula::fd-to-storage::${storage_server}",
      description => 'Allow bacula-fd access to the bacula-storage',
      chain       => 'bacula-sd',
      saddr       => $bacula::public_addresses,
    }
  } elsif $ensure == 'absent' {
    file { '/etc/bacula':
      ensure  => absent,
      purge   => true,
      force   => true,
      recurse => true;
    }
  }

  ensure_packages ( [
    'bacula-fd',
    'bacula-common',
  ], {
    ensure => $package_ensure
  })

  service { 'bacula-fd':
    ensure    => $service_ensure,
    enable    => $service_enable,
    hasstatus => true,
  }
  dsa_systemd::override { 'bacula-fd':
    ensure  => $ensure,
    content => @(EOF),
      [Unit]
      After=network-online.target unbound.service
      [Service]
      ExecStart=
      ExecStart=/usr/sbin/bacula-fd -c $CONFIG -f -u bacula -k
      | EOF
  }

  exec { 'bacula-fd restart-when-idle':
    path        => '/usr/bin:/usr/sbin:/bin:/sbin',
    command     => "sh -c 'setsid /usr/local/sbin/bacula-idle-restart ${port_fd} bacula-fd &'",
    refreshonly => true,
    subscribe   => [ File[$bacula::ssl_server_cert], File[$bacula::ssl_client_cert] ],
    require     => [ File['/usr/local/sbin/bacula-idle-restart'], File['/etc/bacula/fd-conf.d/empty.conf'] ],
  }

  file { '/etc/bacula/bacula-fd.conf':
    ensure  => $ensure,
    content => template('bacula/bacula-fd.conf.erb'),
    mode    => '0640',
    owner   => root,
    group   => bacula,
    notify  => Exec['bacula-fd restart-when-idle'],
  }
  file { '/etc/bacula/fd-conf.d/empty.conf':
    ensure  => $ensure,
    content => '',
    mode    => '0440',
    group   => bacula,
    notify  => Exec['bacula-fd restart-when-idle'],
  }
  file { '/usr/local/sbin/bacula-backup-dirs':
    ensure => $ensure,
    mode   => '0775',
    source => 'puppet:///modules/bacula/bacula-backup-dirs',
  }
  file { '/usr/local/sbin/postbaculajob':
    ensure => $ensure,
    mode   => '0775',
    source => 'puppet:///modules/bacula/postbaculajob',
  }
  file { '/etc/default/bacula-fd':
    ensure  => $ensure,
    content => template('bacula/default.bacula-fd.erb'),
    mode    => '0400',
    owner   => root,
    group   => root,
    notify  => Service['bacula-fd'],
  }
}
