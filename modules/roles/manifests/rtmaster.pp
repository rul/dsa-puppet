# rt.debian.org
#
# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
class roles::rtmaster (
  String  $db_address,
  Integer $db_port,
) {
  include apache2
  ssl::service { 'rt.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }

  # Note that there is also role specific config in exim4.conf
  exim::vdomain { 'rt.debian.org':
    mail_user  => 'rt',
    mail_group => 'rt',
  }
  exim::dkimdomain { 'rt.debian.org': }

  @@postgres::cluster::hba_entry { "rt-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => 'rtdb',
    user     => 'rtuser',
    address  => $base::public_addresses,
  }
}
