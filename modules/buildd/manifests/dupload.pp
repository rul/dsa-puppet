# dupload configuration
class buildd::dupload {
  package { 'dupload':
    ensure => installed,
  }
  file { '/etc/dupload.conf':
    source  => 'puppet:///modules/buildd/dupload.conf',
    require => Package['dupload'],
  }
}
