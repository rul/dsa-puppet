# a mirror of ports.debian.org
#
# @param listen_addr IP addresses to have apache listen on
# @param onion_service provide the onion service from this host
class roles::ports_mirror(
  Array[Stdlib::IP::Address] $listen_addr = [],
  Boolean $onion_service = false,
) {
  include roles::archvsync_base
  include apache2

  $_enclosed_addresses = empty($listen_addr) ? {
    true    => ['*'],
    default => enclose_ipv6($listen_addr),
  }
  $vhost_listen = $_enclosed_addresses.map |$a| { "${a}:80" } .join(' ')

  $mirror_basedir_prefix = hiera('role_config__mirrors.mirror_basedir_prefix')
  $archive_root = "${mirror_basedir_prefix}debian-ports"

  apache2::site { '010-ftp.ports.debian.org':
    site    => 'ftp.ports.debian.org',
    content => template('roles/apache-ftp.ports.debian.org.erb'),
  }

  if $onion_service {
    $onion_addr = empty($listen_addr) ? {
      true    => $base::public_address,
      default => filter_ipv4($listen_addr)[0]
    }
    if ! $onion_addr {
      fail("Do not have a useable address for the onionservice on ${::hostname}.  Is \$listen_addr empty or does it not have an IPv4 address?.")
    }

    onion::service { 'ftp.ports.debian.org':
      port           => 80,
      target_port    => 80,
      target_address => $onion_addr,
    }
  }

  Ferm::Rule::Simple <<| tag == 'ssh::server::from::syncproxy' |>>
}
