# = Class: roles::dbmaster
#
# Setup for db.debian.org master host
#
# == Sample Usage:
#
#   include roles::dbmaster
#
class roles::dbmaster {
  include apache2

  include roles::pubsub::parameters

  $rabbit_password = $roles::pubsub::parameters::rabbit_password

  ssl::service { 'db.debian.org':
    notify   => [ Exec['service apache2 reload'],
                  Service['slapd'] ],
    key      => true,
    tlsaport => [443, 389, 636],
  }

  file { '/etc/ldap/db.debian.org.key':
    ensure  => present,
    mode    => '0440',
    group   => 'openldap',
    content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.letsencrypt_dir"]) + "/db.debian.org.key") %>'),
    links   => follow,
  }

  roles::pubsub::config { 'generate':
    key      => 'dsa-udgenerate',
    exchange => dsa,
    topic    => 'dsa.ud.replicate',
    vhost    => dsa,
    username => $::fqdn,
    password => $rabbit_password
  }

  service { 'slapd':
    ensure => running,
  }

  ssh::keygen {'dsa': }
  ssh::authorized_key_add { 'dbmaster::puppetmaster::nagios-build':
    target_user => 'puppet',
    command     => '/srv/puppet.debian.org/sync/bin/puppet-ssh-wrap draghi.debian.org nagiosconfig',
    key         => $facts['dsa_key'],
    collect_tag => 'puppetmaster',
  }

  exim::vdomain { 'db.debian.org':
    mail_user  => 'mail_db',
    mail_group => 'nogroup',
  }

  exim::dkimdomain { 'db.debian.org': }

  ferm::rule::simple { 'finger':
    port => 'finger',
  }
  ferm::rule::simple { 'ldap':
    port => ['ldap', 'ldaps'],
  }

  apache2::site { 'db.debian.org':
    site   => 'db.debian.org.conf',
    source => 'puppet:///modules/roles/dbmaster/apache-db.debian.org.conf',
  }

  concat { '/etc/apache2/conf-available/puppet-restricted-acl.conf':
    mode           => '0444',
    ensure_newline => true,
    warn           => '# This file is maintained with puppet',
    notify         => Exec['service apache2 reload'],
  }
  Concat::Fragment <<| tag == 'debian_org::apt_restricted::apache-acl' |>>
  concat::fragment { 'debian_org::apt_restricted::apache-acl-head':
    target  => '/etc/apache2/conf-available/puppet-restricted-acl.conf',
    order   => '01',
    content => @(EOF)
      <Macro dsa-apt-restricted-acl>
      | EOF
  }
  concat::fragment { 'debian_org::apt_restricted::apache-acl-tail':
    target  => '/etc/apache2/conf-available/puppet-restricted-acl.conf',
    order   => '99',
    content => @(EOF)
      </Macro>
      | EOF
  }
  file { '/etc/apache2/conf-enabled/puppet-restricted-acl.conf':
    ensure => symlink,
    target => '../conf-available/puppet-restricted-acl.conf',
    notify => Exec['service apache2 reload'],
  }
}
