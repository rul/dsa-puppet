class roles::popcon {
  class { 'apache2':
    rlimitmem => 512 * 1024 * 1024,
  }

  include apache2::ssl

  ssl::service { 'popcon.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  apache2::site { 'popcon.debian.org':
    site   => 'popcon.debian.org',
    source => 'puppet:///modules/roles/popcon/popcon.debian.org.conf',
  }

  exim::vdomain { 'popcon.debian.org':
    owner => 'popcon',
    group => 'popcon',
  }
}
