# Backup this cluster
#
# This define causes the cluster to be registered on the backupservers.
#
# Furthermore, if this cluster is managed with postgresql::server and
# do_role and do_hba are set, we create the role and modify the pg_hba.conf file.
#
# Since postgresql::server only supports a single cluster per host, we are moving
# towards our own postgres::cluster, and this define also exports a hba rule for
# those (regardless of the do_hba setting).  If the cluster is managed with
# postgres::cluster and has its manage_hba option set, this will then cause the
# backup hosts to be allowed to replicate.
#
# Regarless of how the cluster is managed, firewall rules are set up to allow
# access from the backup hosts.
#
# @param pg_version      pg version of the cluster
# @param pg_cluster      cluster name
# @param pg_port         port of the postgres cluster
# @param db_backup_role  replication role username
# @param db_backup_role_password     password of the replication role
# @param do_role         create the role (requires setup with postgresql::server)
# @param do_hba          update pg_hba   (requires setup with postgresql::server)
define postgres::backup_cluster(
  String $pg_version,
  String $pg_cluster = 'main',
  Integer $pg_port = 5432,
  String $db_backup_role = lookup('postgres::backup_cluster::db_backup_role'),
  String $db_backup_role_password = hkdf('/etc/puppet/secret', "postgresql-${::hostname}-${$pg_cluster}-${pg_port}-backup_role}"),
  Boolean $do_role = false,
  Boolean $do_hba = false,
) {
  include postgres::backup_source

  $datadir = "/var/lib/postgresql/${pg_version}/${pg_cluster}"
  file { "${datadir}/.nobackup":
    content  => ''
  }

  ## XXX - get these from the roles and ldap
  # backuphost2, storace
  $backup_servers_addrs = ['93.94.130.161/32', '140.211.166.213/32',
                           '2605:bc80:3010:b00:0:deb:166:213/128', '2a02:158:380:280::161/128']

  if $do_role {
    postgresql::server::role { $db_backup_role:
      password_hash => postgresql_password($db_backup_role, $db_backup_role_password),
      replication   => true,
    }
  }
  if $do_hba {
    $backup_servers_addrs.each |String $address| {
      postgresql::server::pg_hba_rule { "debian_backup-${address}":
        description => 'Open up PostgreSQL for backups',
        type        => 'hostssl',
        database    => 'replication',
        user        => $db_backup_role,
        address     => $address,
        auth_method => 'md5',
      }
    }
  }

  postgres::cluster::hba_entry { "backup-replication::${pg_version}::${pg_cluster}":
    pg_version => $pg_version,
    pg_cluster => $pg_cluster,
    pg_port    => $pg_port,
    database   => 'replication',
    user       => $db_backup_role,
    address    => $backup_servers_addrs,
  }
  postgres::backup_server::register_backup_cluster { "backup-role-${::fqdn}}-${pg_port}":
    pg_port     => $pg_port,
    pg_role     => $db_backup_role,
    pg_password => $db_backup_role_password,
    pg_cluster  => $pg_cluster,
    pg_version  => $pg_version,
  }
}
