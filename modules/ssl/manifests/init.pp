class ssl (
	Boolean $insecure_ssl = false
) {
	package { 'openssl':
		ensure   => installed,
	}
	package { 'ssl-cert':
		ensure   => installed,
	}
	package { 'ca-certificates':
		ensure   => installed,
	}

	if $insecure_ssl {
		$extra_ssl_certs_flags = ' --default'
		$ssl_certs_config = 'puppet:///modules/ssl/ca-certificates-global.conf'
	} else {
		$extra_ssl_certs_flags = ''
		$ssl_certs_config = 'puppet:///modules/ssl/ca-certificates.conf'
	}

	file { '/etc/ssl/README':
		mode   => '0444',
		source => 'puppet:///modules/ssl/README',
	}
	file { '/etc/ca-certificates.conf':
		source => $ssl_certs_config,
		notify  => Exec['refresh_normal_hashes'],
	}
	file { '/etc/ca-certificates-debian.conf':
		mode    => '0444',
		source => 'puppet:///modules/ssl/ca-certificates.conf',
		notify  => Exec['refresh_ca_debian_hashes'],
	}
	file { '/etc/ca-certificates-global.conf':
		source => 'puppet:///modules/ssl/ca-certificates-global.conf',
		notify  => Exec['refresh_ca_global_hashes'],
	}

	file { '/etc/ssl/certs/ssl-cert-snakeoil.pem':
		ensure => absent,
		notify => Exec['refresh_normal_hashes'],
	}
	file { '/etc/ssl/private/ssl-cert-snakeoil.key':
		ensure => absent,
	}

	file { '/etc/ssl/servicecerts':
		ensure   => absent,
	}

	file { '/usr/local/share/ca-certificates/debian.org':
		ensure   => absent,
		purge    => true,
		recurse  => true,
		force    => true,
		notify   => [ Exec['refresh_normal_hashes'], Exec['refresh_ca_global_hashes'] ],
	}
	file { '/etc/ssl/certs/README':
		ensure => absent,
	}
	file { '/etc/ssl/ca-debian':
		ensure => directory,
		mode   => '0755',
	}
	file { '/etc/ssl/ca-debian/README':
		ensure => absent,
	}
	file { '/etc/ssl/ca-global':
		ensure => directory,
		mode   => '0755',
	}
	file { '/etc/ssl/ca-global/README':
		ensure => absent,
	}
	file { '/etc/ssl/debian':
		ensure   => directory,
		source   => 'puppet:///files/empty/',
		mode     => '0644', # this works; otherwise all files are +x
		purge    => true,
		recurse  => true,
		force    => true,
	}
	file { '/etc/ssl/debian/certs':
		ensure  => directory,
		mode    => '0755',
	}
	file { '/etc/ssl/debian/crls':
		ensure  => directory,
		mode    => '0755',
	}
	file { '/etc/ssl/debian/certs/thishost.crt':
		content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.auto_clientcerts_dir"]) + "/" + @fqdn + ".client.crt") %>'),
		notify  => Exec['refresh_debian_hashes'],
	}
	file { '/etc/ssl/debian/certs/ca.crt':
		content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.auto_clientcerts_dir"]) + "/ca.crt") %>'),
		notify  => Exec['refresh_debian_hashes'],
	}
	file { '/etc/ssl/debian/crls/ca.crl':
		content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.auto_clientcerts_dir"]) + "/ca.crl") %>'),
	}
	file { '/etc/ssl/debian/certs/thishost-server.crt':
		content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.auto_certs_dir"]) + "/" + @fqdn + ".crt") %>'),
		notify  => Exec['refresh_debian_hashes'],
	}

	file { '/etc/ssl/debian/keys/thishost.key':
		ensure => absent,
	}
	file { '/etc/ssl/debian/keys/thishost-server.key':
		ensure => absent,
	}
	file { '/etc/ssl/debian/keys':
		ensure => absent,
		force    => true,
	}
	file { '/etc/ssl/private/thishost.key':
		content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.auto_clientcerts_dir"]) + "/" + @fqdn + ".key") %>'),
		mode    => '0440',
		group   => ssl-cert,
		require => Package['ssl-cert'],
	}
	file { '/etc/ssl/private/thishost-server.key':
		content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.auto_certs_dir"]) + "/" + @fqdn + ".key") %>'),
		mode    => '0440',
		group   => ssl-cert,
		require => Package['ssl-cert'],
	}

	$updatecacertsdsa = '/usr/local/sbin/update-ca-certificates-dsa'
	if (versioncmp($::lsbmajdistrelease, '9') >= 0) {
		file { $updatecacertsdsa:
			ensure => absent,
		}
		$updatecacerts = '/usr/sbin/update-ca-certificates'
	} else {
		file { $updatecacertsdsa:
			mode   => '0555',
			source => 'puppet:///modules/ssl/update-ca-certificates-dsa',
		}
		$updatecacerts = $updatecacertsdsa
	}

	file { '/etc/apt/apt.conf.d/local-ssl-ca-global':
		mode   => '0444',
		content => template('ssl/local-ssl-ca-global.erb'),
	}


	exec { 'refresh_debian_hashes':
		command     => 'c_rehash /etc/ssl/debian/certs',
		refreshonly => true,
		require     => Package['openssl'],
	}

	exec { 'refresh_normal_hashes':
		# NOTE 1: always use update-ca-certificates to manage hashes in
		#         /etc/ssl/certs otherwise /etc/ssl/ca-certificates.crt will
		#         get a hash overriding the hash that would have been generated
		#         for another certificate ... which is problem, comrade
		# NOTE 2: always ask update-ca-certificates to freshen (-f) the links
		command     => "/usr/sbin/update-ca-certificates --fresh${extra_ssl_certs_flags}",
		refreshonly => true,
		require     => Package['ca-certificates'],
	}
	exec { 'refresh_ca_debian_hashes':
		command     => "${updatecacerts} --fresh --certsconf /etc/ca-certificates-debian.conf --localcertsdir /dev/null --etccertsdir /etc/ssl/ca-debian --hooksdir /dev/null",
		refreshonly => true,
		require     => [
			Package['ca-certificates'],
			File['/etc/ssl/ca-debian'],
			File['/etc/ca-certificates-debian.conf'],
			File[$updatecacertsdsa],
		]
	}
	exec { 'refresh_ca_global_hashes':
		command     => "${updatecacerts} --fresh --default --certsconf /etc/ca-certificates-global.conf --etccertsdir /etc/ssl/ca-global --hooksdir /dev/null",
		refreshonly => true,
		require     => [
			Package['ca-certificates'],
			File['/etc/ssl/ca-global'],
			File['/etc/ca-certificates-global.conf'],
			File[$updatecacertsdsa],
		]
	}

}
