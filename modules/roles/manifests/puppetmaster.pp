# our puppet master role
class roles::puppetmaster {
  include puppetmaster

  ssh::authorized_key_collect { 'dsa_wiki_buildhost':
    target_user => 'dsa',
    collect_tag => 'puppetmaster',
  }

  ssh::authorized_key_collect { 'puppetmaster':
    target_user => 'puppet',
    collect_tag => 'puppetmaster',
  }
}
