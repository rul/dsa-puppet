class hardware::fixes {
	case $::hostname {
		bm-bl1,bm-bl2,bm-bl4,bm-bl5,bm-bl6,lobos,villa: {
			concat::fragment { 'puppet-crontab--hp-health':
				target => '/etc/cron.d/puppet-crontab',
				#order  => '100',
				content  => @(EOF)
					@hourly root  (for i in `seq 1 5`; do timeout 25 hpasmcli -s help && break; sleep 5; service hp-health stop; sleep 5; service hp-health start; sleep 10; done) > /dev/null 2>/dev/null
					| EOF
			}
		}
	}
}
