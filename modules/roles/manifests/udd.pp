# UDD
class roles::udd {
  class { 'apache2':
    rlimitmem => 512 * 1024 * 1024,
  }

  ssl::service { 'udd.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }

  include roles::buildd_master::db_guest_access

  class { 'roles::udd::db_guest_access':
    database        => ['udd', 'udd-dev'],
    address         => ['127.0.0.1', '::1'],
    connection_type => 'host',
  }
}
