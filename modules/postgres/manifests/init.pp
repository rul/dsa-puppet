# base class for a host with postgres installed
#
# This just provides some common infrastructure and monitoring.
class postgres {
  $ensure = ($::postgres) ? {
    true    => 'present',
    default => 'absent'
  }

  # postgresql::server provides per-cluster munin setup, so the
  # server-level setup here isn't needed
  if defined(Class['roles::postgresql::server']) {
    $munin_ensure = 'absent'
  } else {
    $munin_ensure = $ensure
  }

  munin::check { 'postgres_bgwriter':
    ensure => $munin_ensure,
  }
  munin::check { 'postgres_connections_db':
    ensure => $munin_ensure,
  }
  munin::check { 'postgres_cache_ALL':
    ensure => $munin_ensure,
    script => 'postgres_cache_'
  }
  munin::check { 'postgres_querylength_ALL':
    ensure => $munin_ensure,
    script => 'postgres_querylength_'
  }
  munin::check { 'postgres_size_ALL':
    ensure => $munin_ensure,
    script => 'postgres_size_'
  }

  file { '/usr/share/munin/plugins/postgres_wal_traffic_':
    ensure => $ensure,
    source => 'puppet:///modules/postgres/munin/postgres_wal_traffic_',
    mode   => '0755',
  }

  file { '/usr/share/munin/plugins/postgres_wrapper':
    ensure => $ensure,
    source => 'puppet:///modules/postgres/munin/postgres_wrapper',
    mode   => '0755',
  }

  file { '/etc/munin/plugin-conf.d/local-postgres':
    ensure => $ensure,
    source => 'puppet:///modules/postgres/plugin.conf',
  }
  file { '/usr/local/sbin/dsa-restart-all-idle-postgres':
    ensure => $ensure,
    source => 'puppet:///modules/postgres/dsa-restart-all-idle-postgres',
    mode   => '0555',
  }

  file { '/usr/local/sbin/pg-maybe-checkpoint':
    ensure => $ensure,
    source => 'puppet:///modules/postgres/pg-maybe-checkpoint',
    mode   => '0555',
  }
  file { '/etc/sudoers.d/puppet-postgres':
    ensure  => $ensure,
    mode    => '0440',
    content => template('postgres/sudoers.erb'),
  }
  if $::postgres {
    package { 'libdbd-pg-perl':
      ensure => installed,
    }

    concat::fragment { 'puppet-crontab--postgres-pg-maybe-checkpoint':
      target  => '/etc/cron.d/puppet-crontab',
      content => @("EOF")
        42 */6 * * * postgres chronic /usr/local/sbin/pg-maybe-checkpoint
        | EOF
    }
  }
}
