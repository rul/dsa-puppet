# the ipsec tunnel between fasolo and storace, so that our backups work
# despite all the evil firewalls that might try to block our traffic.
class profile::ipsec::fasolo_storace {

  # Use the first ipv4 address from LDAP, since the puppet fact is not always
  # the IP address we want to use.  For instance, for storace $::facts['ipaddress']
  # is 172.29.170.1 (from bond1) instead of 93.94.130.161 from eth0.
  $public_ipaddress = getfromhash($deprecated::nodeinfo, 'misc', 'v4_ldap')[0]

  # we do ipsec on the backend since it travels over other people's switching infra
  ipsec::network { "fasolo_storace":
    peer_ipaddress => $public_ipaddress,
  }
}
