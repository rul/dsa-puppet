class deprecated {

	$localinfo = yamlinfo('*')
	$nodeinfo  = nodeinfo($::fqdn)
	$allnodeinfo = allnodeinfo('sshRSAHostKey ipHostNumber', 'purpose mXRecord physicalHost purpose')

	if ! ($localinfo.size > 0) {
		fail('Cannot learn localinfo.')
	}
	if ! ($nodeinfo.size > 0) {
		fail('Cannot learn nodeinfo.')
	}
	if ! ($allnodeinfo.size > 0) {
		fail('Cannot learn allnodeinfo.')
	}
}
