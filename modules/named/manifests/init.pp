class named {
  file { '/usr/share/munin/plugins/bind':
    ensure => file,
    source => 'puppet:///modules/named/munin/bind',
    mode   => '0755',
  }

  munin::check { 'bind': }

  munin::conf { 'puppet-bind':
    content => @(EOF)
      # THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
      [bind*]
      group bind
      env.logfile /var/log/bind9/named-query.log
      | EOF
  }

  package { 'bind9':
    ensure => installed
  }

  service { 'bind9':
    ensure => running,
  }

  ferm::rule { '00-dsa-bind-no-ddos-any':
    domain      => '(ip ip6)',
    description => 'Allow nameserver access',
    rule        => 'proto udp dport 53 mod string from 32 to 64 algo bm hex-string \'|0000ff0001|\' jump DROP'
  }

  ferm::rule { 'dsa-bind-notrack':
    domain      => '(ip ip6)',
    description => 'NOTRACK for nameserver traffic',
    table       => 'raw',
    chain       => 'PREROUTING',
    rule        => 'proto (tcp udp) dport 53 jump NOTRACK'
  }

  ferm::rule { 'dsa-bind-notrack-out':
    domain      => '(ip ip6)',
    description => 'NOTRACK for nameserver traffic',
    table       => 'raw',
    chain       => 'OUTPUT',
    rule        => 'proto (tcp udp) sport 53 jump NOTRACK'
  }

  file { '/var/log/bind9':
    ensure => directory,
    owner  => bind,
    group  => bind,
    mode   => '0775',
  }

  file { '/etc/bind/named.conf.options':
    content => template('named/named.conf.options.erb'),
    notify  => Service['bind9'],
  }

  file { '/etc/bind/named.conf.puppet-shared-keys':
    mode    => '0640',
    content => template('named/named.conf.puppet-shared-keys.erb'),
    owner   => root,
    group   => bind,
    notify  => Service['bind9'],
  }

  concat { '/etc/bind/named.conf.puppet-misc':
    notify => Service['bind9'],
  }
  concat::fragment { 'dsa-named-conf-puppet-misc---header':
    target  => '/etc/bind/named.conf.puppet-misc',
    order   => '000',
    content => @(EOF)
      // THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
      | EOF
  }
}
