class named::geodns inherits named {
  munin::check { 'bind_views':
    script => bind
  }

  package { 'geoip-database':
    ensure => installed,
  }

  file { '/etc/bind/':
    ensure  => directory,
    group   => bind,
    mode    => '2755',
    require => Package['bind9'],
    notify  => Service['bind9'],
  }
  file { '/etc/bind/geodns':
    ensure => directory,
    mode   => '0755',
  }
  file { '/etc/bind/named.conf.local':
    source => 'puppet:///modules/named/common/named.conf.local',
    notify => Service['bind9'],
  }
  if (versioncmp($::lsbmajdistrelease, '9') >= 0) {
    file { '/etc/bind/named.conf.acl':
      source => 'puppet:///modules/named/common/named.conf.acl',
      notify => Service['bind9'],
    }
  } else {
    file { '/etc/bind/named.conf.acl':
      source => 'puppet:///modules/named/common/named.conf.acl.bind99',
      notify => Service['bind9'],
    }
  }
  file { '/etc/bind/geodns/zonefiles':
    ensure => directory,
    owner  => geodnssync,
    group  => geodnssync,
    mode   => '2755',
  }
  file { '/etc/bind/geodns/named.conf.geo':
    source => 'puppet:///modules/named/common/named.conf.geo',
    notify => Service['bind9'],
  }
  file { '/etc/bind/geodns/trigger':
    mode   => '0555',
    source => 'puppet:///modules/named/common/trigger',
  }
  file { '/etc/cron.d/dsa-boot-geodnssync': ensure => absent; }
  concat::fragment { 'puppet-crontab--geodns-boot':
    target  => '/etc/cron.d/puppet-crontab',
    content => @(EOF)
      @reboot geodnssync sleep 1m && /etc/bind/geodns/trigger > /dev/null
      | EOF
  }

  ferm::rule::simple { '01-dsa-bind':
    description => 'Allow nameserver access',
    proto       => ['udp', 'tcp'],
    port        => 'domain',
  }
}
