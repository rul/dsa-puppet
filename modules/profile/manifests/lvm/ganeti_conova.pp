# LVM config for the arm servers that make up ganeti-conova.debian.org
class profile::lvm::ganeti_conova {
  class { 'lvm':
    global_filter  => '[ "a|^/dev/md[0-9]*$|", "r/.*/" ]',
    issue_discards => true,
  }
}
