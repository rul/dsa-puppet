class roles::loghost {
	include syslog_ng::loghost

	ferm::rule { 'dsa-syslog':
		domain          => '(ip ip6)',
		description     => 'Allow syslog access',
		rule            => '&SERVICE_RANGE(tcp, 5140, $HOST_DEBIAN)'
	}
	ferm::rule { 'fastly-syslog':
		description     => 'Allow syslog access',
		rule            => '&SERVICE_RANGE(tcp, 5141, $HOST_FASTLY)'
	}
}
