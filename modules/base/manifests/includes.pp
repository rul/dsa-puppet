class base::includes {
  include roles::mta

  include munin
  include syslog_ng
  include sudo
  include ssh
  include debian_org
  include monit
  include time
  include ssl
  include hardware
  include nagios::client
  include resolv
  include roles
  include motd
  include unbound
  include bacula::client
  include grub
  include multipath
  include popcon
  include portforwarder
  include postgres
  include haveged
  include huge_mem
  include tcp_bbr
  include certregen::client
}
