# LVM config for the blades that make up ganeti.grnet.debian.org
class profile::lvm::ganeti_grnet {
  class { 'lvm':
    global_filter  => '[ "a|^/dev/sda[0-9]*$|", "r/.*/" ]',
  }
}
