# video.debian.net archive master
class roles::video_archive {
  package { 'git-annex': ensure => installed, }

  rsync::site { 'video.debian.net':
    source      => 'puppet:///modules/roles/video_archive/rsyncd.conf',
  }
}
