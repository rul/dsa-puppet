# LVM config for the 2018 dell servers that make up ganeti.manda.debian.org
class profile::lvm::ganeti_manda {
  class { 'lvm':
    global_filter  => '[ "a|^/dev/sd[abc][0-9]*$|", "r/.*/" ]',
    issue_discards => true,
  }
}
