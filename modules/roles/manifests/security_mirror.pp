# security mirror
#
# @param listen_addr IP addresses to have rsync listen on
# @param onion_service provide the onion service from this host
# @param healthcheck_name name to access this node in the health checker
class roles::security_mirror(
  Array[Stdlib::IP::Address] $listen_addr = [],
  Boolean $onion_service = false,
  Optional[String] $healthcheck_name = undef,
){
  include roles::archvsync_base
  include apache2
  include apache2::expires
  include apache2::rewrite

  $enclosed_addresses_rsync = empty($listen_addr) ? {
    true    => ['[::]'],
    default => enclose_ipv6($listen_addr),
  }
  $_enclosed_addresses = empty($listen_addr) ? {
    true    => ['*'],
    default => enclose_ipv6($listen_addr),
  }
  $vhost_listen = $_enclosed_addresses.map |$a| { "${a}:80" } .join(' ')

  apache2::site { '010-security.debian.org':
    site    => 'security.debian.org',
    content => template('roles/security_mirror/security.debian.org.erb')
  }

  apache2::site { '015-rsync.security.debian.org':
    site    => 'rsync.security.debian.org',
    content => template('roles/security_mirror/rsync.security.debian.org.erb')
  }

  rsync::site { 'security':
    source      => 'puppet:///modules/roles/security_mirror/rsyncd.conf',
    max_clients => 100,
    binds       => $enclosed_addresses_rsync,
  }

  if $onion_service {
    $onion_addr = empty($listen_addr) ? {
      true    => $base::public_address,
      default => filter_ipv4($listen_addr)[0]
    }
    if ! $onion_addr {
      fail("Do not have a useable address for the onionservice on ${::hostname}.  Is \$listen_addr empty or does it not have an IPv4 address?.")
    }

    onion::service { 'security.debian.org':
      port           => 80,
      target_port    => 80,
      target_address => $onion_addr,
    }
  }

  Ferm::Rule::Simple <<| tag == 'ssh::server::from::security_master' |>>

  mirror_health::service { 'security':
    this_host_service_name => $healthcheck_name,
    url                    => 'http://security.backend.mirrors.debian.org/debian-security/dists/stable/updates/Release',
    health_url             => 'http://security.backend.mirrors.debian.org/_health',
  }

  # security abusers
  #  198.108.67.48 DoS against our rsync service
  ferm::rule { 'dsa-security-abusers':
    prio => '005',
    rule => 'saddr ( 198.108.67.48/32 ) DROP',
  }
}
