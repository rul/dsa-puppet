class bootserver(
  String $dhcpd_conf,
  String $interface = 'eth1',
  Enum['present','absent'] $ensure = 'present',
) {
  $package_ensure = $ensure ? { 'present' => 'installed', 'absent' => 'purged' }
  $service_ensure = $ensure ? { 'present' => 'running', 'absent'  => 'stopped' }
  $service_enable = $ensure ? { 'present' => true, 'absent' => false }

  # Make sure the service is stopped before we purge packages
  $package_require = $ensure ? { 'present' => [], 'absent' => [Service['isc-dhcp-server']] }

  ensure_packages ( [
    'tftpd-hpa',
    'isc-dhcp-server',
  ], {
    ensure  => $package_ensure,
    require => $package_require,
  })

  if $ensure == 'present' {
    ferm::rule { 'dsa-bootserver-tftp':
      domain => '(ip)',
      rule   => "proto udp interface ${interface} dport tftp jump ACCEPT",
    }
  }

  service { 'isc-dhcp-server':
    ensure  => $service_ensure,
    enable  => $service_enable,
    require => [
      File['/etc/default/isc-dhcp-server'],
      File['/etc/dhcp/dhcpd.conf'],
    ],
  }
  file { '/etc/default/isc-dhcp-server':
    ensure  => $ensure,
    content => @("EOF"),
                   INTERFACESv4="${interface}"
                   INTERFACESv6=""
                   | EOF
    notify  => Service['isc-dhcp-server'],
  }
  file { '/etc/dhcp/dhcpd.conf':
    ensure  => $ensure,
    content => $dhcpd_conf,
    notify  => Service['isc-dhcp-server'],
  }
}
