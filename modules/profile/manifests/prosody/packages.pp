# This class manages the extra packages required by the rtc team to manage
# prosody

class profile::prosody::packages {

  $packages_to_install = [
    'python3-lxml',
  ]

  ensure_packages($packages_to_install, { ensure => 'installed' })

}
