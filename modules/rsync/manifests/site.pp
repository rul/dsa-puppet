# an rsync site, systemd socket activated
define rsync::site (
  Array[String] $binds = ['[::]'],
  Optional[String] $source = undef,
  Optional[String] $content = undef,
  Integer $max_clients = 200,
  Enum['present','absent'] $ensure = 'present',
  Optional[String] $sslname = undef,
) {
  include rsync

  $fname_real_rsync = "/etc/rsyncd-${name}.conf"
  $fname_real_stunnel = "/etc/rsyncd-${name}-stunnel.conf"

  file { $fname_real_rsync:
    ensure  => $ensure,
    content => $content,
    source  => $source,
  }

  dsa_systemd::socket_service { "rsyncd-${name}":
    ensure          => $ensure,
    service_content => template('rsync/systemd-rsyncd.service.erb'),
    socket_content  => template('rsync/systemd-rsyncd.socket.erb'),
    require         => File[$fname_real_rsync],
  }

  if $sslname {
    file { $fname_real_stunnel:
      ensure  => $ensure,
      content => template('rsync/systemd-rsyncd-stunnel.conf.erb'),
      require => File["/etc/ssl/debian/certs/${sslname}.crt-chained"],
    }

    dsa_systemd::socket_service { "rsyncd-${name}-stunnel":
      ensure          => $ensure,
      service_content => template('rsync/systemd-rsyncd-stunnel.service.erb'),
      socket_content  => template('rsync/systemd-rsyncd-stunnel.socket.erb'),
      require         => File[$fname_real_stunnel],
    }

    ferm::rule { "rsync-${name}-ssl":
      domain      => '(ip ip6)',
      description => 'Allow rsync access',
      rule        => '&SERVICE(tcp, 1873)',
    }

    $certdir = hiera('paths.letsencrypt_dir')
    dnsextras::tlsa_record{ "tlsa-${sslname}-1873":
      zone     => 'debian.org',
      certfile => [ "${certdir}/${sslname}.crt" ],
      port     => 1873,
      hostname => $sslname,
    }
  }
}
