# Install and enable (or disable) an apache config snippet
#
# @param source  source of the apache conf file
# @param content content of the apache conf file
# @param ensure  present or absent
define apache2::config (
  Optional[String] $source = undef,
  Optional[String] $content = undef,
  Enum['present','absent'] $ensure = 'present',
) {
  include apache2

  case $ensure {
    present: {
      if ! ($source or $content) {
        fail ( "No configuration found for ${name}" )
      }
    }
    absent:  {}
    default: { fail ( "Unknown ensure value: ${ensure}" ) }
  }

  file { "/etc/apache2/conf-available/${name}.conf":
    ensure  => $ensure,
    content => $content,
    source  => $source,
    require => Package['apache2'],
    notify  => Exec['service apache2 reload'],
  }

  $link_ensure = $ensure ? {
    present => link,
    absent  => absent
  }

  file { "/etc/apache2/conf-enabled/${name}.conf":
    ensure => $link_ensure,
    target => "../conf-available/${name}.conf",
    notify => Exec['service apache2 reload'],
  }
}
