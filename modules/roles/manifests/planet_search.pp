class roles::planet_search {
  include apache2
  ssl::service { 'planet-search.debian.org': notify  => Exec['service apache2 reload'], key => true, }
}
