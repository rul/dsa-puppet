#
# bacula director node
#
class roles::bacula::director(
) {
  include bacula::director

  $pg_server = lookup('bacula::director::db_address')
  $pg_port   = lookup('bacula::director::db_port')

  @@postgres::cluster::hba_entry { 'bacula-dir':
    tag      => "postgres::cluster::${pg_port}::hba::${pg_server}",
    pg_port  => $pg_port,
    database => 'bacula',
    user     => ['bacula', "bacula-${::hostname}-reader", 'nagios'],
    address  => $base::public_addresses,
  }
}
