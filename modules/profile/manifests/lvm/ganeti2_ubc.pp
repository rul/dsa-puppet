# LVM config for the ~2016 blades that make up ganeti2.ubc.debian.org
class profile::lvm::ganeti2_ubc {
  class { 'lvm':
    global_filter  => '[ "a|^/dev/sda[0-9]*$|", "r/.*/" ]',
  }
}
