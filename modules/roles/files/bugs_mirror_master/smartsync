#!/usr/bin/python

# sync updates against bugs.debian.org to a mirror host.
#
# This script uses the linux kernel's inotify support to get notified
# about new and renamed files.  It collects a list of files and then
# calls rsync to copy the files to the remote server.
#
# This does not guarantee perfect syncronisation, for instance we
# explicitly do ignore file removals, but on the other hand it is
# lightweight and fast.  Full rsync runs over the complete tree still
# will have to be done regularly, tho maybe not ever 5 minutes anymore.


# Copyright (c) 2008, 2010, 2020 Peter Palfrader
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import time
import os
import sys
import getopt
import thread
import threading
import logging
import logging.handlers
import subprocess
import Queue
import tempfile
import fnmatch
import signal
import traceback
import fcntl

from pyinotify import WatchManager, ThreadedNotifier, ProcessEvent
import pyinotify

try:
   import yaml
except ImportError:
   import syck as yaml

class INotifyHandler(ProcessEvent):
   def __init__(self, filter, filter_override, include_filter):
      ProcessEvent.__init__(self)
      self.log = logging.getLogger("INotifyHandler")
      #self.log.setLevel(logging.DEBUG)
      self.filter = filter
      self.filter_override = filter_override
      self.include_filter = include_filter

   def filter_enqueue(self, event):
      path = os.path.join(event.path, event.name)
      if event.dir:
         self.log.debug("ignoring %s"%event)
         return

      filtered = False
      for f in self.filter:
         if fnmatch.fnmatch(path, f):
            filtered = True
            break
      if filtered:
         for f in self.filter_override:
            if fnmatch.fnmatch(path, f):
               filtered = False
               break
         if filtered:
            self.log.debug("ignoring %s"%event)
            return

      include_matched = False
      for f in self.include_filter:
         if fnmatch.fnmatch(path, f):
            include_matched = True
            break

      if not include_matched:
         self.log.debug("ignoring %s (no match in include-filter)"%event)
         return

      self.log.debug("Queueing: %s"%event)
      inotify_event_queue.put(event)

   def process_IN_CREATE(self, event):
      self.filter_enqueue(event)
   def process_IN_MOVED_TO(self, event):
      self.filter_enqueue(event)
   def process_IN_CLOSE_WRITE(self, event):
      self.filter_enqueue(event)

def list_replace(list, token, value):
   return map(lambda s: s.replace(token, value), list)

class Syncer(threading.Thread):
   def __init__(self, sync_config):
      threading.Thread.__init__(self)

      self.log = logging.getLogger("Syncer")
      #self.log.setLevel(logging.DEBUG)
      self.config = sync_config

      # time to wait before running an rsync since the last file changed
      if not 'min_wait' in self.config: self.config['min_wait'] = 15
      # time to wait at most since the first file changed
      if not 'max_wait' in self.config: self.config['max_wait'] = 120
      # do not run incremental syncs more often than this
      if not 'min_interval' in self.config: self.config['min_interval'] = 30
      # at most run a full sync once every 15 minutes
      if not 'min_full_wait' in self.config: self.config['min_full_wait'] = 60*15
      # run a full rsync at least every 8 hours
      if not 'max_full_wait' in self.config: self.config['max_full_wait'] = 60*60*8

      self.last_full = 0
      self.rsync_runs_failed = False   # can only be cleared by full rsync runs
      self.last_rsync_ok = True

      # start with a full sync.
      self.full_sync()

   def get_event(self, block = True, timeout = None):
      e = inotify_event_queue.get(block, timeout)
      if not type(e) == pyinotify.Event:
         self.log.debug("found a non-event in queue, exiting thread")
         thread.exit()
      return e


   def syncloop(self):
      while True:
         e = self.get_event()
         to_process = [e]

         # wait until at least MIN_WAIT seconds are past, when one
         # file is update others usually follow soon.
         # under no circumstances wait for longer than MAX_WAIT.
         time_first = time.time()
         while True:
            waited = time.time() - time_first
            if waited > self.config['max_wait']:
               break
            # we are supposed to wait for at least _min_wait_ after the last file was touched
            to_wait = self.config['min_wait']
            # also, we do not want to run a sync more often than _min_interval_
            to_wait = max(to_wait, self.config['min_interval']-waited)
            # but not longer tham _max_wait_ since we got the first file
            to_wait = min(to_wait, self.config['max_wait']-waited)
            # and just to be sure, wait for at least one second, never negative values
            to_wait = max(to_wait, 1)
            try:
               e = self.get_event(True, to_wait)
            except Queue.Empty:
               break
            to_process.append(e)

         self.sync(to_process)

   def run(self):
      try:
         self.syncloop()
      except SystemExit:
         pass
      except:
         self.log.exception('Sync thread: ')
         thread.interrupt_main()

   def handle_rsync_return(self, ret, full):
      self.last_rsync_ok = True

      if ret == 0:
         self.log.info("sync completed successfully")
      elif ret == 24:
         self.log.info("sync finished.  some source files have vanished.")
      else:
         self.log.warn("rsync exited with exit code %d\n"% ret)
         self.last_rsync_ok = False

      if full and self.last_rsync_ok:
         self.rsync_runs_failed = False
      if not self.last_rsync_ok:
         self.rsync_runs_failed = True


   def run_rsync(self, command, full=False):
      if full:
         self.log.info("doing full sync")
      else:
         self.log.info("doing incremental sync")
      rsync = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
      while True:
         line = rsync.stdout.readline()
         if line == '': break
         line = line.rstrip()
         self.log.debug("rsync: %s" % line)
      rsync.wait()
      ret = rsync.returncode
      self.handle_rsync_return(rsync.returncode, full)

   def sync(self, events):
      self.incremental_sync(events)
      # well, we could do the full sync *instead* of the incremental
      # but doing the incremental first and then, maybe, the full has two
      # advantages:
      #  o doing the incremental first means the remote side is up-to-date
      #    sooner since in all likelyhood the full sync doesn't change much
      #    that is relevant.
      #  o The other reason why this is smart is that after a failed sync run
      #    in the past we start doing full syncs once the first incremental
      #    completed successfully again.  This way we do the recovery sync run
      #    much sooner.
      if self.want_full_sync():
         self.full_sync()

   def want_full_sync(self):
      if os.path.exists(self.config['fullsyncflag']):
         os.unlink(self.config['fullsyncflag'])
         return True

      now = time.time()
      waited = now - self.last_full
      # if the last full rsync run was hours ago, try one now
      if waited >= self.config['max_full_wait']:
         return True
      # never run a full sync if we just did one a few minutes ago
      if waited <= self.config['min_full_wait']:
         return False
      # if the last (incremental) rsync run succeeded, but there
      # have been failed rsyncs since the last full, run a full one
      # now
      if self.last_rsync_ok and self.rsync_runs_failed:
         return True
      return False

   def full_sync(self):
      self.run_rsync(self.config['full'], True)
      self.last_full = time.time()
      return True

   def incremental_sync(self, events):
      files = map(lambda e: os.path.join(e.path, e.name), events)
      files = set(files) # remove dups
      files = [fn+"\n" for fn in files if (os.path.exists(fn) and fn != self.config['fullsyncflag'])]
      if len(files) == 0:
         self.log.info("all changed files have disappeared")
         return
      f = tempfile.NamedTemporaryFile()
      f.writelines(files)
      f.flush()
      command = list_replace(self.config['incremental'], '%%FILENAME%%', f.name)
      self.run_rsync(command)

def usage(err = False):
   if err:
      f = sys.stderr
      exit = 1
   else:
      f = sys.stdout
      exit = 0

   f.write("Usage: %s --config=<f> [--quiet|-q [--quiet|-q]] [--foreground|-f] [--stdout]\n" % (sys.argv[0]))
   f.write("       %s --help|-h\n" % (sys.argv[0]))
   f.write("With one --quiet only log INFO and above, with two only log WARN and above.\n")
   f.write("With --stdout log to stdout instead of files (implies --foreground).\n")
   sys.exit(exit)

def addWatchers(log, wm, monitorlist):
   mask = pyinotify.IN_MOVED_TO | pyinotify.IN_CREATE | pyinotify.IN_CLOSE_WRITE
   for f in monitorlist:
      rec = False
      if f.endswith('/'):
         rec = True
         f = f[:-1]
         log.info("Monitoring %s recursively"% f)
      else:
         log.info("Monitoring %s"% f)

      wd = wm.add_watch(f, mask, rec=rec, auto_add=rec)
      for key in wd:
         if wd[key] < 0:
            log.warn("Adding watch for %s failed."% key)
   log.info("All watchers set up.")

def readConfig(conffile):
   return yaml.safe_load(open(conffile).read())

def setupLogger(conf, stdount, quiet):
   if quiet >= 2:
      loglevel = level=logging.WARN
   elif quiet >= 1:
      loglevel = level=logging.INFO
   else:
      loglevel = level=logging.DEBUG
   logging.getLogger('').setLevel(loglevel)


   if stdount:
      handler = logging.StreamHandler()
   else:
      handler = logging.handlers.TimedRotatingFileHandler(conf['filename'], 'midnight', 1, conf['keep'])
   handler.setFormatter( logging.Formatter('%(asctime)s %(levelname)s %(name)s: %(message)s') )
   logging.getLogger('').addHandler(handler)


def daemonize(log):
   try:
      if os.fork() > 0: os._exit(0)
   except OSError, error:
      log.error('fork #1 failed: %d (%s)' % (error.errno, error.strerror))
      os.exit(1)
   os.setsid()
   try:
      if os.fork() > 0: os._exit(0)
   except OSError, error:
      log.error('fork #2 failed: %d (%s)' % (error.errno, error.strerror))
      os.exit(1)

def on_sigterm(signalnum, frame):
   raise KeyboardInterrupt('SIGTERM')

def main():
   global inotify_event_queue

   try:
      # --comment is ignored, but we can put stuff in the command line this way
      opts, args = getopt.getopt(sys.argv[1:], "hqf", ["help", "config=", "quiet", "foreground", "stdout", "comment="])
   except getopt.GetoptError, err:
      print str(err)
      usage(True)
   if len(args) > 0:
      usage(True)

   quiet = 0
   foreground = False
   stdout = False
   configfile = None
   for opt, arg in opts:
      if opt in ("--help", "-h"):
         usage()
      elif opt in ("--quiet", "-q"):
         quiet = quiet + 1
      elif opt in ("--foreground", "-f"):
         foreground = True
      elif opt in ("--stdout"):
         foreground = True
         stdout = True
      elif opt in ("--config"):
         configfile = arg

   if configfile is None:
      usage(True)
   config = readConfig(configfile)
   os.chdir(config['base'])

   setupLogger(config['log'], stdout, quiet)
   log = logging.getLogger("Main")
   if not foreground:
      daemonize(log)

   inotify_event_queue = Queue.Queue(0)

   wm = WatchManager()
   st = fcntl.fcntl(wm._fd, fcntl.F_GETFD)
   fcntl.fcntl(wm._fd, fcntl.F_SETFD, st | fcntl.FD_CLOEXEC)

   if not 'filter' in config:
      config['filter'] = []
   if not 'filter_override' in config:
      config['filter_override'] = []
   if not 'include_filter' in config:
      config['include_filter'] = [ '*' ]
   notifier = ThreadedNotifier(wm,
                               INotifyHandler(config['filter'],
                                              config['filter_override'],
                                              config['include_filter']))

   signal.signal(signal.SIGTERM, on_sigterm)
   syncer = None
   try:
      addWatchers(log, wm, config['monitor'])
      notifier.start()
      syncer = Syncer(config['sync'])
      syncer.start()

      # interrupt_main() that our child threads raise here in main when they
      # run into errors will not break out of sleeps, so make lots of small
      # sleeps.  This is broken, but hey.
      while True:
         time.sleep(60)
         if not syncer.isAlive():
            log.error("sync thread died.")
            sys.exit(1)
         if not notifier.isAlive():
            log.error("notifier thread died.")
            sys.exit(1)

   except KeyboardInterrupt:
      log.info("Shutting down")
      inotify_event_queue.put("please stop, ktnx")
   finally:
      if notifier.isAlive():
         notifier.stop()
      if syncer is not None and syncer.isAlive():
         syncer.stop()


if __name__ == "__main__":
   main()

# vim:set et:
# vim:set ts=3:
# vim:set shiftwidth=3:
