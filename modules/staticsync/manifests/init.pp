# A base class for staticsync.  This owns the configuration variables, which
# should be set by hiera.
class staticsync (
  String $user,
  String $basedir,
  # for ssh/firewalling purposes
  Array[Stdlib::IP::Address] $public_addresses = $base::public_addresses,
) {
  if ! $user {
    fail('Variable $user is not set.  Please provide a value (for instance via hiera).')
  }
  if ! $basedir {
    fail('Variable $basedir is not set.  Please provider a value (for instance via hiera).')
  }
}
