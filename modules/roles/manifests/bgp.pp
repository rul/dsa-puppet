class roles::bgp(
  Array[Stdlib::IP::Address] $peers,
){
  ferm::rule::simple { 'dsa-bgp':
    description => 'Allow BGP from peers',
    port        => 'bgp',
    saddr       => $peers,
  }

  file { '/etc/network/interfaces.d/anycasted':
    content => template('roles/anycast/interfaces.erb')
  }
}
