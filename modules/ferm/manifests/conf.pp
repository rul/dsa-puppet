#
define ferm::conf (
  $source=undef,
  $content=undef,
  $ensure=present,
  $prio='xx-10',
) {
  include ferm

  case $ensure {
    present: {
      if ! ($source or $content) {
        fail ( "No configuration found for ${name}" )
      }
    }
    absent:  {}
    default: { fail ( "Unknown ensure value: '${ensure}'" ) }
  }

  if ($source and $content) {
    fail ( "Can't define both source and content for ${name}" )
  }

  $fname = "/etc/ferm/conf.d/${prio}_${name}.conf"

  if $content {
    file { $fname:
        ensure  => $ensure,
        mode    => '0400',
        content => $content,
        notify  => Exec['ferm reload'],
    }
  } else {
    file { $fname:
        ensure => $ensure,
        mode   => '0400',
        source => $source,
        notify => Exec['ferm reload'],
    }
  }
}
