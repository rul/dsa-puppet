# wanna-build
#
# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
# @param qa_buildlogchecks_db_address     hostname of the postgres server for this service
# @param qa_buildlogchecks_db_port        port of the postgres server for this service
class roles::buildd_master (
  String  $qa_buildlogchecks_db_address,
  Integer $qa_buildlogchecks_db_port,
  String  $db_address = $roles::buildd_master::params::db_address,
  Integer $db_port    = $roles::buildd_master::params::db_port,
) inherits roles::buildd_master::params {
  include apache2
  include roles::sso_rp

  ssl::service { 'buildd.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  ssl::service { 'auth.buildd.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }

  ssh::authorized_key_collect { 'buildd-master':
    target_user => 'wb-buildd',
    collect_tag => 'buildd_master',
  }

  exim::vdomain { 'buildd.debian.org':
    owner => 'wbadm',
    group => 'wbadm',
  }

  class { 'roles::buildd_master::db_guest_access':
    database => ['wanna-build', 'wanna-build-test'],
  }

  @@postgres::cluster::hba_entry { "buildd_master-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => ['wanna-build', 'wanna-build-test'],
    user     => 'all',
    address  => $base::public_addresses,
  }

  # The UDD database is used to display FTBFS bugs on the web interface
  include roles::udd::db_guest_access

  include roles::postgresql::ftp_master_dak_replica::db_guest_access::ubc

  @@postgres::cluster::hba_entry { "qa-buildlogchecks-${::fqdn}":
    tag      => "postgres::cluster::${qa_buildlogchecks_db_port}::hba::${qa_buildlogchecks_db_address}",
    pg_port  => $qa_buildlogchecks_db_port,
    database => 'qa-buildlogchecks',
    user     => 'qa-buildlogchecks',
    address  => $base::public_addresses,
  }
}
