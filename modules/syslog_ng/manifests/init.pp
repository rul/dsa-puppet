# configure syslog-ng
class syslog_ng {
  package { 'syslog-ng':
    ensure => installed
  }

  $query = 'nodes[certname] { resources { type = "Class" and title = "Roles::Loghost" } }'
  $loghosts = sort(puppetdb_query($query).map |$value| { $value["certname"] })
  $v6only = ($base::public_address == undef)

  service { 'syslog-ng':
    ensure    => running,
    hasstatus => false,
    pattern   => 'syslog-ng',
  }

  file { '/etc/syslog-ng/syslog-ng.conf':
    content => template('syslog_ng/syslog-ng.conf.erb'),
    require => Package['syslog-ng'],
    notify  => Service['syslog-ng']
  }
  file { '/etc/default/syslog-ng':
    source  => 'puppet:///modules/syslog_ng/syslog-ng.default',
    require => Package['syslog-ng'],
    notify  => Service['syslog-ng']
  }
  file { '/etc/logrotate.d/syslog-ng':
    source  => 'puppet:///modules/syslog_ng/syslog-ng.logrotate',
    require => Package['syslog-ng']
  }

  if $::systemd {
    file { '/etc/systemd/system/syslog-ng.service':
      source => 'puppet:///modules/syslog_ng/syslog-ng.service',
      notify => Exec['systemctl daemon-reload'],
    }

    file { '/etc/systemd/system/syslog.service':
      ensure => absent,
      notify => Exec['systemctl daemon-reload'],
    }
  }
}
