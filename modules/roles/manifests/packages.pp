class roles::packages {
  class { 'apache2':
    rate_limit => true,
  }

  ssl::service { 'packages.debian.org': notify  => Exec['service apache2 reload'], key => true, }

  # Note that there is also role specific config in exim4.conf
  exim::vdomain { 'packages.debian.org':
    owner => 'pkg_user',
    group => 'pkg_maint',
  }
}
